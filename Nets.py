# -*- coding: utf-8 -*-

import numpy

import chainer
import chainer.functions as F
import chainer.links as L
from chainer import cuda

from Const import BOS_ID, EOS_ID


def sequence_embed(embed, xs):
    x_len = [len(x) for x in xs]
    x_section = numpy.cumsum(x_len[:-1])
    ex = embed(F.concat(xs, axis=0))
    exs = F.split_axis(ex, x_section, 0)
    return exs


def convert(seq, device):
    xs = numpy.pad([seq], ((0, 0), (1, 0)), 'constant', constant_values=BOS_ID)
    ys = numpy.pad([seq], ((0, 0), (0, 1)), 'constant', constant_values=EOS_ID)
    if device > -1:
        xs = cuda.to_gpu(xs, device)
        ys = cuda.to_gpu(ys, device)
    return xs, ys


class RNNLM(chainer.Chain):
    def __init__(self,n_vocab, opts):
        super(RNNLM, self).__init__()
        with self.init_scope():
            self.embed = L.EmbedID(n_vocab, opts.emb)
            if opts.rnn == 'LSTM':
                self.rnn = L.NStepLSTM(
                                opts.layers, opts.emb, opts.units, opts.dout)
                self.wo = L.Linear(opts.units, n_vocab)
            elif opts.rnn == 'BiLSTM':
                self.rnn = L.NStepBiLSTM(
                                opts.layers, opts.emb, opts.units, opts.dout)
                self.wo = L.Linear(2*opts.units, n_vocab)

    def __call__(self, xs, ys):
        batchsize = len(xs)
        h = sequence_embed(self.embed, xs)
        _, _, os = self.rnn(None, None, h)

        concat_os = F.concat(os, axis=0)
        concat_ys = F.concat(ys, axis=0)
        loss = F.sum(F.softmax_cross_entropy(
                self.wo(concat_os), concat_ys, reduce='no')) / batchsize

        chainer.report({'loss': loss.data}, self)
        n_words = concat_ys.shape[0]
        prep = self.xp.exp(loss.data * batchsize / n_words)
        chainer.report({'prep': prep}, self)
        return loss

    def perplexity(self, seq, device):
        xs, ys = convert(seq, device)
        bs, n_words = xs.shape
        assert bs == 1 # batchsize must be 1

        with chainer.no_backprop_mode(), chainer.using_config('train', False):
            h = sequence_embed(self.embed, xs)
            _, _, os = self.rnn(None, None, h)
            
            concat_os = F.concat(os, axis=0)
            concat_ys = F.concat(ys, axis=0)
            probs = F.softmax_cross_entropy(
                    self.wo(concat_os), concat_ys, reduce='no')
            perp = self.xp.exp(F.sum(probs).data / n_words)
            return perp
