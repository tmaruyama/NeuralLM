# -*- coding: utf-8 -*-

def model_opts(parser):
    # Embedding options
    group = parser.add_argument_group('Model-Embeddings')
    group.add_argument('--emb', type=int, default=200,
        help='number of embedding units')

    # Encoder-Decoder options
    group = parser.add_argument_group('Model-RNN')
    group.add_argument('--layers', type=int, default=2, 
        help='number of rnn layers')
    group.add_argument('--units', type=int, default=200,
        help='number of hidden units of rnn')
    group.add_argument('--rnn', default='LSTM', choices=['LSTM', 'BiLSTM'], 
        help='type of recurrent neural network')
    group.add_argument('--dropout', type=float, default=0.2,
                        help='Dropout ratio of LSTM')


def train_opts(parser):
    # General options
    group = parser.add_argument_group('General')
    group.add_argument('--gpuid', '-g', type=int, default=-1,
        help='GPU ID')
    group.add_argument('--model', '-m', default='sample',
        help='file name prefix to serialize')
    group.add_argument('--save-dir', '-s', default='demo',
        help='directory to output the result')
    group.add_argument('--minfreq', '-f', type=int, default=0,
        help='minimum frequency of using words in dataset')
    group.add_argument('--maxlen', type=int, default=70,
        help='maximum length of training sentences')
    group.add_argument('--minlen', type=int, default=4,
        help='minimum length of training sentences')

    # Data options
    group = parser.add_argument_group('Data')
    group.add_argument('--train', default='./bccwj_50.train',
       help='sentence list for training')
    group.add_argument('--vocabsize', type=int, default=50000,
        help='vocabulary size')

    # Optimization options
    group = parser.add_argument_group('Optimization')
    group.add_argument('--batchsize', type=int, default=4,
        help='Number of examples in each mini-batch')
    group.add_argument('--epoch', '-e', type=int, default=20,
        help='Number of sweeps over the dataset to train')
    group.add_argument('--dout', type=float, default=0.2)
    group.add_argument('--optim', default='Adam', choices=['SGD', 'Adam'])

    # Logging options
    group = parser.add_argument_group('Logging')
    parser.add_argument('--log-interval', type=int, default=10,
        help='number of iteration to show log')


def compute_perplexity_opts(parser):
    group = parser.add_argument_group('General')
    group.add_argument('--model', '-m', default='./demo/sample-e20.model',
        help='model for complutation of perplexity')
    group.add_argument('--test', default='./bccwj_50.dev',
        help='sentence list for computation of perplexity')
    group.add_argument('--batchsize', type=int, default=2)
    group.add_argument('--gpuid', '-g', type=int, default=-1,
        help='GPU ID')
