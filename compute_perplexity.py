#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import pickle
import os

from chainer import serializers
from chainer import cuda

from Utils import word2id
from Opts import compute_perplexity_opts
from Nets import RNNLM


def parse_args():
    parser = argparse.ArgumentParser(
        description='calculate_perplexity.py',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    compute_perplexity_opts(parser)
    opts = parser.parse_args()
    return opts


def main():
    opts = parse_args()
    
    # Load model's parameters and a vocabulary
    prefix = os.path.join(os.path.dirname(opts.model), 
              ''.join(os.path.basename(opts.model).split('-')[:-1]))
    params = pickle.load(open('{}.opts'.format(prefix), 'br'))
    vocab = [w.rstrip() for w in open('{}.vocab'.format(prefix), 'r')]
    
    # Setup models
    vocab_size = len(vocab) + 3 # 3 means number of special tags such as "UNK"
    model = RNNLM(vocab_size, params)
    serializers.load_npz(opts.model, model)
    if opts.gpuid >= 0:
        cuda.get_device(opts.gpuid).use()
        model.to_gpu(opts.gpuid)

    # Setup a data
    test_data = word2id(opts.test, vocab)

    # calculate perplexity
    for src in test_data:
        perp = model.perplexity(src, opts.gpuid)
        print(perp)


if __name__ == '__main__':
    main()
