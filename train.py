#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import pickle
import os
import numpy
import matplotlib

import chainer
from chainer import training
from chainer import cuda
from chainer.training import extensions
from chainer import serializers

from Utils import get_vocab, save_vocab, word2id
from Opts import train_opts, model_opts
from Nets import RNNLM
from Const import BOS_ID, EOS_ID, UNK_ID


matplotlib.use('Agg')


class SaveModel(chainer.training.Extension):
    trigger = 1, 'epoch'
    priority = chainer.training.PRIORITY_WRITER

    def __init__(self, model, save_dir, name):
        self.model = model
        self.save_dir = save_dir
        self.name = name

    def __call__(self, trainer):
        model_name = \
            '{}-e{}.model'.format(self.name, str(trainer.updater.epoch))
        save_path = os.path.join(self.save_dir, model_name)
        serializers.save_npz(save_path, self.model)


def lm_convert(batch, device):
    def to_device_batch(batch):
        if device is None:
            return batch
        elif device < 0:
            return [chainer.dataset.to_device(device, x) for x in batch]
        else:
            concat = numpy.concatenate(batch, axis=0)
            sections = numpy.cumsum([len(x)
                                     for x in batch[:-1]], dtype=numpy.int32)
            concat_dev = chainer.dataset.to_device(device, concat)
            batch_dev = cuda.cupy.split(concat_dev, sections)
            return batch_dev

    xs = to_device_batch(
            [numpy.pad(seq, (1, 0), 'constant', constant_values=BOS_ID) 
             for seq in batch])

    ys = to_device_batch(
            [numpy.pad(seq, (0, 1), 'constant', constant_values=EOS_ID)
             for seq in batch])
    return xs, ys


def parse_args():
    parser = argparse.ArgumentParser(
        description='train.py',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    train_opts(parser)
    model_opts(parser)
    opts = parser.parse_args()
    return opts


def calculate_unknown_ratio(data):
    n_unk = sum((s == UNK_ID).sum() for s in data)
    total = sum(s.size for s in data)
    return n_unk / total


def main():
    opts = parse_args()
    os.mkdir(opts.save_dir)
    save_path = os.path.join(opts.save_dir, '{}.opts'.format(opts.model))
    f = open(save_path, 'bw')
    pickle.dump(opts, f)

    vocab = get_vocab(opts.train, opts.vocabsize, opts.minfreq)
    save_path = os.path.join(opts.save_dir, '{}.vocab'.format(opts.model))
    save_vocab(save_path, vocab)

    train_data = word2id(opts.train, vocab)
    train_data = [s for s in train_data
                  if opts.minlen <= len(s) <= opts.maxlen]
    unk_ratio = calculate_unknown_ratio(train_data)

    print('*** Details of training data ***')
    print('Vocabulary size: %d' % len(vocab))
    print('Train data size: %d' % len(train_data))
    print('Train data unknown ratio: %f' % unk_ratio)
    print('')
    
    # Setup models
    vocab_size = len(vocab) + 3 #  3 means number of special tags such as "UNK"
    model = RNNLM(vocab_size, opts)
    if opts.gpuid >= 0:
        cuda.get_device(opts.gpuid).use()
        model.to_gpu(opts.gpuid)

    # Setup optimizer
    if opts.optim == 'Adam':
        optimizer = chainer.optimizers.Adam()
    elif opts.optim == 'SGD':
        optimizer = chainer.optimizers.SGD()
    optimizer.setup(model)

    # Setup iterator
    train_iter = chainer.iterators.SerialIterator(train_data, opts.batchsize)

    # Setup updater
    updater = training.updaters.StandardUpdater(
                train_iter, optimizer, converter=lm_convert, device=opts.gpuid)

    # Setup trainer
    trainer = training.Trainer(updater, (opts.epoch, 'epoch'), opts.save_dir)
    trainer.extend(extensions.LogReport(
        trigger=(opts.log_interval, 'iteration')))
    trainer.extend(extensions.PrintReport(
        ['epoch', 'iteration', 'main/loss', 'main/prep', 'elapsed_time']),
        trigger=(opts.log_interval, 'iteration'))
    trainer.extend(
        SaveModel(model, opts.save_dir, opts.model), trigger=(1, 'epoch'))
    trainer.extend(extensions.PlotReport(
        ['main/loss', 'main/perp'], x_key='iteration', file_name='log.png'))

    print('start training')
    trainer.run()



if __name__ == '__main__':
    main()
